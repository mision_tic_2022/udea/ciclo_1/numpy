

import numpy as np
from numpy import random as r

matriz = np.array([ [1,2,3,4], [4,3,2,1] ])
print(matriz)


matriz_flotante = np.array( [ [1,2], [3.14,4.5], ['A', 'B'] ] )
#print(matriz_flotante)

#matriz_ceros = np.zeros([2, 2])
#print(matriz_ceros)

#matriz_unos = np.ones((2,4))
#print(matriz_unos)

#Valores aleatorios
nombres = ['Juan', 'María', 'Juliana', 'Andrés', 'Kevin']
numeros = [10,20,30,40,50]

ganadores = r.choice( numeros, size=r.choice([1,3], p=[0.9, 0.1]), p=[0.1, 0.1, 0.5, 0.1, 0.2 ], replace=False )
print('Los ganadores de la rifa son: ', ganadores)

numeros_aleatorios = r.choice( range(45), size=[45], replace=False )
print(numeros_aleatorios)


matriz_a = np.array([ [10,20,30,40,50], [90,80,70,60,50] ])
matriz_b = np.array([ [40,10,60,74,50], [100,10,30,20,90] ])

print(matriz_a[0])

lista: list = list( matriz_a[0] )
lista.index(30)

suma = np.add(matriz_a, matriz_b)
print(suma)
suma = matriz_a + matriz_b

resta = matriz_a - matriz_b
resta = np.subtract(matriz_a, matriz_b)
print(resta)

multiplicacion = matriz_a * matriz_b
multiplicacion = np.multiply(matriz_a, matriz_b)
print(multiplicacion)

matriz_a = np.array([ [1,2,3], [3,2,1] ])
matriz_b = np.array([ [20,10], [30,40], [50,60] ])

resultado = np.dot( matriz_a, matriz_b )
print(resultado)